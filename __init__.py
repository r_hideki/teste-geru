from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory

from sqlalchemy import engine_from_config

from .models.mymodel import DBSession, Base

def main(global_config, **settings):
    my_session_factory = SignedCookieSessionFactory(
        'itsaseekreet')
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config = Configurator(settings=settings,
                          root_factory='projeto1.models.mymodel.Root',
                          session_factory=my_session_factory)
    config.include('pyramid_chameleon')
    config.add_route('desafio', '/')
    config.add_route('quotes', '/quotes')
    config.add_route('quote_random', '/quotes/random')
    config.add_route('quote_number', '/quotes/{quote_number}')
    config.add_route('lista_sessoes', '/lista')
    config.add_static_view('deform_static', 'deform:static/')
    config.scan('.views')
    return config.make_wsgi_app()