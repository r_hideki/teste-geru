import requests, json

def get_quotes():
    response = requests.get("https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/quotes")
    if response.status_code != 200:
        print('Ocorreu o seguinte erro na solicitação: {}'.format(response.content))
    else:
        return json.loads(response.content)


def get_quote(quote_number):
    response = requests.get(
        "https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/quotes/{}".format(quote_number))
    if response.status_code != 200:
        print('Ocorreu o seguinte erro na solicitação: {}'.format(response.content))
    else:
        return json.loads(response.content)