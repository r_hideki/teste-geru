import colander
import deform.widget
import transaction
import random
import datetime
from ..static import getquotes

from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config, view_defaults
from sqlalchemy.sql.expression import func

from ..models.mymodel import DBSession, Sessao


class WikiPage(colander.MappingSchema):
    title = colander.SchemaNode(colander.String())
    body = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.RichTextWidget()
    )


@view_defaults(renderer='default.pt')
class WikiViews(object):

    def __init__(self, request):
        self.request = request

#    @property
#    def counter(self):
#        global sid
#        session = self.request.session
#        sid = DBSession.query(func.max(Sessao.sessaoid))
#        if 'counter' not in session:
#            session['counter'] = sid + 1
#        sid = session['counter']
#        return session['counter']

    @view_config(route_name='desafio')
    def desafio(self):
        session = self.request.session
        if 'counter' not in session:
            results = DBSession.query(func.max(Sessao.sessaoid)).first()
            new_session_id = results[0] + 1
            session['counter'] = new_session_id
            DBSession.add(Sessao(sessaoid=new_session_id,
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='desafio'))
        else:
            DBSession.add(Sessao(sessaoid=session['counter'],
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='desafio'))
        transaction.commit()
        return {'re': 'Desafio Web 1.0'}

    @view_config(route_name='quotes', renderer='quotes.pt')
    def quotes(self):
        session = self.request.session
        if 'counter' not in session:
            results = DBSession.query(func.max(Sessao.sessaoid)).first()
            new_session_id = results[0] + 1
            session['counter'] = new_session_id
            DBSession.add(Sessao(sessaoid=new_session_id,
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='quotes'))
        else:
            DBSession.add(Sessao(sessaoid=session['counter'],
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='quotes'))
        transaction.commit()

        frases = getquotes.get_quotes()['quotes']
        return dict(frases=frases)

    @view_config(route_name='quote_number')
    def quote_number(self):
        session = self.request.session
        if 'counter' not in session:
            results = DBSession.query(func.max(Sessao.sessaoid)).first()
            new_session_id = results[0] + 1
            session['counter'] = new_session_id
            DBSession.add(Sessao(sessaoid=new_session_id,
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='quote_number'))
        else:
            DBSession.add(Sessao(sessaoid=session['counter'],
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='quote_number'))
        transaction.commit()

        numero = int(self.request.matchdict['quote_number'])
        print(getquotes.get_quote(numero)['quote'])
        return {'re': 'Quote number ' + self.request.matchdict['quote_number'] + ' - ' +
                      getquotes.get_quote(numero)['quote']}

    @view_config(route_name='quote_random')
    def quote_random(self):
        session = self.request.session
        if 'counter' not in session:
            results = DBSession.query(func.max(Sessao.sessaoid)).first()
            new_session_id = results[0] + 1
            session['counter'] = new_session_id
            DBSession.add(Sessao(sessaoid=new_session_id,
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='quote_random'))
        else:
            DBSession.add(Sessao(sessaoid=session['counter'],
                                 datahora=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                 pagina='quote_random'))
        transaction.commit()

        frases = getquotes.get_quotes()['quotes']
        x = 0
        for item in frases:
            x = x + 1
        aleatorio = random.randint(1, x)
        print(getquotes.get_quote(aleatorio)['quote'])
        return {'re': 'Quote number ' + str(aleatorio) + ' - ' + getquotes.get_quote(aleatorio)['quote']}

    @view_config(route_name='lista_sessoes', renderer='lista.pt')
    def lista_sessoes(self):
        pages = DBSession.query(Sessao).order_by(Sessao.num_reg)
        print('****PRINT DO PAGES****')
        print(pages)
        return dict(pages=pages)
